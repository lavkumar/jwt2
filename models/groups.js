const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema(
{
    name: { type: String},
    desc: { type: String},
    username: { type: String},
    email: { type: String},
    password: { type: String},
    isAdmin: {
        type: Boolean,
        
    },
},{
   timestamps:true
});
module.exports=mongoose.model("group",groupSchema)