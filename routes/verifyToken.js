const jwt =require("jsonwebtoken");

const verifyToken =(req,res,next)=>{
    const authHeader =req.headers.authorization;
    if(authHeader){
        const token = authHeader.split(" ")[1]
        jwt.verify(token,process.env.jwt,(err,group)=>{
            if(err) res.status(403).json("token is not match");
            req.group =group;
          
            next();
        })
    }else{
        return res.status(401).json("you are not authenicated")
    }
};


const verifyTokenAndAuthorization = (req,res,next)=>{
    verifyToken(req,res,()=> {
        if(req.group.id === req.params.id || req.params.isAdmin){
            next();

        }else{
            res.status(403).json("you are not dddallowed to do that!")
        }
    })
}


const verifyTokenAndAdmin = (req,res,next)=>{
    verifyToken(req,res,()=> {
        if(req.group.isAdmin){
            next();

        }else{
            res.status(403).json("you are not allowed to do that!")
        }
    })
}
module.exports = { 
    verifyToken,
    verifyTokenAndAuthorization ,
    verifyTokenAndAdmin
}